package org.giorgi.gorelishvili.export.common;

public class Keys {
	public static final String AMOUNT = "amount";
	public static final String DATE = "date";
	public static final String DATETIME = "datetime";
	public static final String ALLOW_WRAP = "allow_wrap";
}
