package org.giorgi.gorelishvili.export;

import org.giorgi.gorelishvili.export.common.TestCellBuilder;
import org.giorgi.gorelishvili.export.common.TestSheetWriter;
import org.giorgi.gorelishvili.export.writer.TestFastWriter;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
 	TestCellBuilder.class,
	TestFastWriter.class,
	TestSheetWriter.class
})
public class JunitTestSuite {
}